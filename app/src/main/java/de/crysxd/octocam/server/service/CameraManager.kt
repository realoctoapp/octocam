package de.crysxd.octocam.server.service

import android.content.Context
import androidx.camera.core.Camera
import androidx.camera.core.CameraSelector
import androidx.camera.core.TorchState
import androidx.camera.core.UseCase
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.distinctUntilChanged
import de.crysxd.octocam.repository.OctoCamSettingsRepository
import timber.log.Timber

class CameraManager(
    private val context: Context,
    private val lifecycleOwner: LifecycleOwner,
    private val onError: (Throwable) -> Unit
) {

    private var camera: Camera? = null
    private val mutableLightState = MediatorLiveData<Boolean>()
    var liveLightState = mutableLightState.distinctUntilChanged()
    private var targetLightState = false
    var lightState: Boolean
        get() = liveLightState.value == true
        set(value) {
            Timber.i("Torch: $value")
            camera?.cameraControl?.enableTorch(value)
            targetLightState = value
        }

    init {
        mutableLightState.postValue(false)
    }

    fun updateCamera(vararg useCase: UseCase?) {
        Timber.i("Updating camera")
        val uc = useCase.mapNotNull { it }.toTypedArray()

        val cameraProviderFuture = ProcessCameraProvider.getInstance(context)
        cameraProviderFuture.addListener({
            // Used to bind the lifecycle of cameras to the lifecycle owner
            val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()

            // Select back camera as a default
            val cameraSelector = CameraSelector.Builder().addCameraFilter {
                listOf(it[OctoCamSettingsRepository.value.cameraId])
            }.build()

            try {
                // Unbind use cases before rebinding
                cameraProvider.unbindAll()

                // Bind use cases to camera
                if (uc.isNotEmpty()) {
                    val c = cameraProvider.bindToLifecycle(
                        lifecycleOwner,
                        cameraSelector,
                        *uc
                    )

                    // Init torch state
                    if (targetLightState) {
                        c.cameraControl.enableTorch(true)
                        Timber.i("Torch turned on")
                    }

                    // Update sensor orientation
                    val rotation = c.cameraInfo.sensorRotationDegrees
                    if (rotation != OctoCamSettingsRepository.value.sensorRotationDegrees) {
                        Timber.i("Camera orientation: $rotation")
                        OctoCamSettingsRepository.update {
                            copy(sensorRotationDegrees = rotation)
                        }
                    }

                    // Update torch state
                    mutableLightState.removeSource(c.cameraInfo.torchState)
                    mutableLightState.addSource(c.cameraInfo.torchState) {
                        Timber.i("Torch state: $it/${c.cameraInfo.hasFlashUnit()}")
                        mutableLightState.postValue(
                            when {
                                !c.cameraInfo.hasFlashUnit() -> null
                                it == TorchState.ON -> true
                                it == TorchState.OFF -> false
                                else -> null
                            }
                        )
                    }
                    camera = c
                    Timber.i("Camera started")
                } else {
                    Timber.i("Camera stopped")
                }
            } catch (exc: Exception) {
                Timber.e(exc)
                onError(exc)
            }
        }, ContextCompat.getMainExecutor(context))
    }
}