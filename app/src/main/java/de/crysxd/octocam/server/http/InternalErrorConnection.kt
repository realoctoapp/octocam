package de.crysxd.octocam.server.http

import java.io.OutputStreamWriter
import java.net.Socket
import java.util.concurrent.ExecutorService

class InternalErrorConnection(
    socket: Socket,
    executor: ExecutorService,
    doOnClose: () -> Unit = {},
) : HttpConnection(
    HttpRequest("GET", "/", emptyMap(), emptyMap(), null),
    socket,
    executor,
    doOnClose
) {

    override fun writeResponse(writer: OutputStreamWriter): Boolean {
        val message = "<h1>Internal server error</h1><p>Somethig went wrong</p>"
        writer.writeDefaultHeaders(500)
        writer.writeContentHeaders("text/html", message.length)
        writer.closeHeaders()
        writer.write(message)
        return true
    }
}