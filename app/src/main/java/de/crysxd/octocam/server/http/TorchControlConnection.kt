package de.crysxd.octocam.server.http

import androidx.lifecycle.asFlow
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import de.crysxd.octocam.server.service.MainServiceBinder
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeoutOrNull
import java.io.OutputStreamWriter
import java.net.Socket
import java.util.concurrent.ExecutorService

class TorchControlConnection(
    socket: Socket,
    httpRequest: HttpRequest,
    private val mainServiceBinder: MainServiceBinder,
    executor: ExecutorService,
    doOnClose: () -> Unit = {},
) : HttpConnection(
    httpRequest,
    socket,
    executor,
    doOnClose
) {

    private val moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()

    override fun writeResponse(writer: OutputStreamWriter): Boolean = runBlocking {
        val torchFlow = mainServiceBinder.getServiceState().asFlow().map {
            it.lightsOn == true
        }
        val torchStateBefore = torchFlow.first()
        val torchStateAfter = if (httpRequest.method == "POST" && httpRequest.body != null) {
            val json = String(httpRequest.body)
            moshi.adapter(RequestBody::class.java).fromJson(json)?.torchOn?.let { after ->
                timber.i("Received request to set torch state: $after")
                mainServiceBinder.setLightOn(after)

                // Wait for torch to get expected state, up to 500ms
                withTimeoutOrNull(500) {
                    timber.i("Waiting for torch to have state $after")
                    torchFlow.filter { it == after }.first()
                    timber.i("Torch switched to state $after")
                    after
                }
            } ?: torchStateBefore
        } else {
            // Not a POST, state does not change
            torchStateBefore
        }

        timber.i("Responding with torch state: $torchStateAfter")
        val response =
            moshi.adapter(ResponseBody::class.java).toJson(ResponseBody(torchOn = torchStateAfter))
        writer.writeDefaultHeaders(200)
        writer.writeContentHeaders("application/json", response.length)
        writer.closeHeaders()
        writer.write(response)
        true
    }

    data class RequestBody(
        val torchOn: Boolean?
    )

    data class ResponseBody(
        val torchOn: Boolean,
    )
}