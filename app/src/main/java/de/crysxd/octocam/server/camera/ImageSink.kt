package de.crysxd.octocam.server.camera

import android.graphics.Bitmap
import java.io.ByteArrayOutputStream
import java.util.concurrent.ExecutorService

interface ImageSink {
    val imageExecutor: ExecutorService
    val byteBuffer: ByteArrayOutputStream
    fun onByteBufferUpdated(format: Bitmap.CompressFormat)
}