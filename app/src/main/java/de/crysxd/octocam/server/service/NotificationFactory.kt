package de.crysxd.octocam.server.service

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.core.app.NotificationCompat
import androidx.lifecycle.LifecycleService
import de.crysxd.octocam.R
import de.crysxd.octocam.ui.main.MainActivity

@OptIn(ExperimentalAnimationApi::class)
class NotificationFactory(private val context: Context) {

    fun createServerActiveNotification(connectionCount: Int): Notification {
        val channelId = createNotificationChannel()
        return NotificationCompat.Builder(context, channelId)
            .setContentTitle("OctoCam server running")
            .setContentText(if (connectionCount == 0) "Waiting for connections" else "$connectionCount connections")
            .setSmallIcon(R.drawable.ic_notification)
            .setContentIntent(
                PendingIntent.getActivity(
                    context,
                    1,
                    Intent(context, MainActivity::class.java),
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
                    } else {
                        PendingIntent.FLAG_UPDATE_CURRENT
                    }
                )
            )
            .setSilent(true)
            .build()
    }

    private fun createNotificationChannel() =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val id = "service"
            val manager =
                context.getSystemService(LifecycleService.NOTIFICATION_SERVICE) as NotificationManager
            manager.createNotificationChannel(
                NotificationChannel(id, "Service", NotificationManager.IMPORTANCE_HIGH)
            )
            id
        } else {
            "notachannel"
        }

}