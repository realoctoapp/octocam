package de.crysxd.octocam.server.service

import android.os.Binder
import androidx.camera.core.Preview
import androidx.lifecycle.LiveData
import de.crysxd.octocam.models.ServiceState

abstract class MainServiceBinder : Binder() {
    abstract fun getServiceState(): LiveData<ServiceState>
    abstract fun bindPreview(uc: Preview)
    abstract fun unbindPreview()
    abstract fun stopService()
    abstract fun setLightOn(on: Boolean)
}