package de.crysxd.octocam.server.http

import java.io.OutputStreamWriter
import java.net.Socket
import java.util.concurrent.ExecutorService

class ServerBusyHttpConnection(
    socket: Socket,
    executor: ExecutorService,
    private val maxConnectionCount: Int,
    doOnClose: () -> Unit,
) : HttpConnection(
    HttpRequest("GET", "/", emptyMap(), emptyMap(), null),
    socket,
    executor,
    doOnClose
) {

    override fun writeResponse(writer: OutputStreamWriter): Boolean {
        val message =
            "<h1>Service unavailable</h1><p>Maximum consecutive connection count of <b>$maxConnectionCount</b> exceeded</p>"
        writer.writeDefaultHeaders(503)
        writer.writeContentHeaders("text/html", message.length)
        writer.closeHeaders()
        writer.write(message)
        return true
    }
}