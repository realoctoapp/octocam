package de.crysxd.octocam.server.camera

import de.crysxd.octocam.logging.TaggedTree

class FpsCounter(tag: String) {
    companion object {
        private const val FPS_HISTORY_THRESHOLD = 3000
        private const val FPS_LOG_INTERVAL = 5000
    }

    private var lastFrame: Long? = null
    private val frameTimes = mutableListOf<Pair<Long, Long>>()
    private var lastFpsLog: Long = 0
    private var timber = TaggedTree("FpsCounter[$tag]")

    val fps: Float
        get() {
            if (frameTimes.isEmpty()) return 0f
            val avgFrameTime = frameTimes.sumOf { it.second } / frameTimes.size.toFloat()
            return 1000 / avgFrameTime
        }

    fun recordFrame() {
        // Calc frame times
        val now = System.currentTimeMillis()
        lastFrame?.let {
            frameTimes.add(now to now - it)
            frameTimes.removeAll { data ->
                data.first < (now - FPS_HISTORY_THRESHOLD)
            }
        }
        lastFrame = System.currentTimeMillis()

        // Log
        fps.takeIf { it > 0 }?.let {
            if (lastFpsLog < (now - FPS_LOG_INTERVAL)) {
                lastFpsLog = now
                timber.i("FPS: %.2f", fps)
            }
        }
    }

    fun throttle(targetFrameRate: Int) {
        val avgFrameTime = frameTimes.sumOf { it.second } / frameTimes.size.toFloat()
        val delayNeededForTarget = 1000 / targetFrameRate
        // x2 so we also limit the next frame right away, otherwise we don't reach the
        // target frame rate
        val extraDelay = (delayNeededForTarget - avgFrameTime) * 2f
        val throttling = extraDelay > 0
        if (throttling) {
            Thread.sleep(extraDelay.toLong())
        }
    }
}