package de.crysxd.octocam.server.http

import android.graphics.Bitmap
import de.crysxd.octocam.server.camera.FpsCounter
import de.crysxd.octocam.server.camera.ImageMediator
import de.crysxd.octocam.server.camera.ImageSink
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.ByteArrayOutputStream
import java.io.OutputStreamWriter
import java.net.Socket
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.TimeUnit

class MjpegStreamHttpConnection(
    httpRequest: HttpRequest,
    socket: Socket,
    executor: ExecutorService,
    doOnClose: () -> Unit,
) : HttpConnection(httpRequest, socket, executor, doOnClose), ImageSink {

    companion object {
        private const val NO_IMAGE_TIMEOUT = 5000L
        private const val NL = "\r\n"
    }

    override val imageExecutor = executor
    override val byteBuffer = ByteArrayOutputStream()
    private var isSingleFrame = false
    private var closeAfterTimeoutJob: Job? = null
    private val boundary = UUID.randomUUID().toString()
    private val fpsCounter = FpsCounter("MjpegStreamHttpConnection/$connectionId")

    override fun writeResponse(writer: OutputStreamWriter): Boolean {
        isSingleFrame = httpRequest.query.containsKey("snapshot")
        writer.writeDefaultHeaders(200)

        // Start sending frame(s)
        ImageMediator.registerForNextImage(this)
        scheduleClose()

        if (isSingleFrame) {
            timber.i("Send partial headers for snapshot")
        } else {
            writer.write("Content-Type: multipart/x-mixed-replace;boundary=$boundary$NL")
            writer.closeHeaders()
            timber.i("Send headers for stream")
        }

        // Do not close connection, we are not done
        return false
    }

    override fun close() {
        ImageMediator.unregisterForNextImage(this)
        closeAfterTimeoutJob?.cancel()
        super.close()
    }

    override fun onByteBufferUpdated(format: Bitmap.CompressFormat) = if (isActive) {
        try {
            closeAfterTimeoutJob?.cancel()

            timber.v("Sending frame (${byteBuffer.size()} byte)")
            imageExecutor.submit {
                // Send frame
                val outputWriter = output.writer()
                if (!isSingleFrame) {
                    outputWriter.write("$NL--$boundary$NL")
                }
                outputWriter.writeContentHeaders(format.mimeType, byteBuffer.size())
                outputWriter.closeHeaders()
                outputWriter.flush()
                byteBuffer.writeTo(output)
                output.flush()

            }.get(LINGER_TIMEOUT_MS.toLong(), TimeUnit.MILLISECONDS)
            timber.v("Frame sent")

            if (isSingleFrame) {
                // Frame send, we are done
                close()
                timber.i("Single frame send, closing connection")
            } else {
                // Register for next frame
                fpsCounter.recordFrame()
                ImageMediator.registerForNextImage(this)
                scheduleClose()
            }

        } catch (e: Exception) {
            Timber.w("Failed to send frame")
            Timber.e(e)
            close()
        }
    } else {
        Timber.v("Closed already, no sending next frame")
    }

    private fun scheduleClose() {
        closeAfterTimeoutJob?.cancel()
        closeAfterTimeoutJob = connectionScope.launch {
            delay(NO_IMAGE_TIMEOUT)
            close()
        }
    }

    @Suppress("Deprecation")
    private val Bitmap.CompressFormat.mimeType
        get() = when (this) {
            Bitmap.CompressFormat.JPEG -> "image/jpeg"
            Bitmap.CompressFormat.PNG -> "image/png"
            Bitmap.CompressFormat.WEBP -> "image/webp"
            else -> throw IllegalStateException("Failed to map mime type for $this")
        }
}
