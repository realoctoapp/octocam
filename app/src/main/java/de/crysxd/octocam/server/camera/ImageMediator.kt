package de.crysxd.octocam.server.camera

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.media.Image
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import de.crysxd.octocam.ext.toJpeg
import de.crysxd.octocam.repository.OctoCamSettingsRepository
import timber.log.Timber
import java.io.ByteArrayOutputStream
import java.util.concurrent.Executors
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock
import kotlin.system.measureTimeMillis

object ImageMediator : ImageAnalysis.Analyzer {

    private val lock = ReentrantLock()
    internal val executor = Executors.newSingleThreadExecutor()
    private val byteBuffer = ByteArrayOutputStream()
    private val fpsCounter = FpsCounter("ImageMediator")
    private val waitingSinks = mutableListOf<ImageSink>()

    // Limiting
    private val targetFrameRate get() = OctoCamSettingsRepository.value.targetFps


    fun registerForNextImage(sink: ImageSink) = lock.withLock {
        waitingSinks.add(sink)
    }

    fun unregisterForNextImage(sink: ImageSink) = lock.withLock {
        Timber.i("Removing $sink as sink")
        waitingSinks.remove(sink)
    }

    @SuppressLint("UnsafeOptInUsageError")
    override fun analyze(imageProxy: ImageProxy) {
        try {
            // Update last image size
            OctoCamSettingsRepository.update {
                copy(lastImageWidth = imageProxy.width, lastImageHeight = imageProxy.height)
            }

            // Skip if nobody is waiting
            if (waitingSinks.isNotEmpty()) {
                Timber.v("Forwarding image to all waiting sinks")
                imageProxy.image?.let { image ->
                    val format = compressImage(image)
                    writeToAllStreams(format)
                }
                rateLimit()
            } else {
                // No sink, throttle
                Timber.v("No waiting sinks, throttling")
                Thread.sleep(100)
            }
        } catch (e: Exception) {
            Timber.e(e)
        } finally {
            // Close image so we get next
            imageProxy.close()
        }
    }

    private fun rateLimit() {
        fpsCounter.recordFrame()
        fpsCounter.throttle(targetFrameRate)
    }

    private fun compressImage(image: Image): Bitmap.CompressFormat {
        measureTimeMillis {
            byteBuffer.reset()
            image.toJpeg(byteBuffer)
        }.let {
            Timber.v("Compressed image with ${image.width}x${image.height}px in ${it}ms")
        }

        return Bitmap.CompressFormat.JPEG
    }

    private fun writeToAllStreams(format: Bitmap.CompressFormat) {
        // Copy to all sinks, lock until done
        lock.withLock {
            waitingSinks.map {
                try {
                    it.imageExecutor.submit {
                        it.byteBuffer.reset()
                        byteBuffer.writeTo(it.byteBuffer)
                        it.onByteBufferUpdated(format)
                    }
                } catch (e: Exception) {
                    Timber.w("Failed to submit image")
                }
            }

            // Reset waiting
            waitingSinks.clear()
        }
    }
}