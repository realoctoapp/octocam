package de.crysxd.octocam.server.http

import androidx.core.net.toUri
import timber.log.Timber
import java.net.Socket
import java.util.concurrent.ExecutorService
import java.util.regex.Pattern

class HttpRequestReader(
    private val socket: Socket,
    private val executor: ExecutorService,
    private val handleRequest: (HttpRequest) -> Unit,
) {

    private val methodPattern: Pattern = Pattern.compile("^([a-zA-Z]+) ")
    private val pathPattern: Pattern = Pattern.compile("^[a-zA-Z]+\\s+(.+)\\s")

    init {
        readRequest()
    }

    private fun readRequest() = executor.submit {
        try {
            // Read request
            val requestLines = mutableListOf<String>()
            val reader = socket.getInputStream()
            var line: String
            do {
                var char: Char
                line = ""
                do {
                    char = reader.read().toChar()
                    line += char
                } while (char != '\n')
                requestLines.add(line.trim())
            } while (line.isNotBlank())
            Timber.v("Request read:\n${requestLines.joinToString("\n")}")

            // Get method and path
            val requestLine =
                requestLines.firstOrNull() ?: throw IllegalArgumentException("Request is empty")
            val method = methodPattern.matcher(requestLine).let {
                if (!it.find() || it.groupCount() < 1) {
                    throw IllegalArgumentException("Unable to parse method from $requestLine")
                }
                it.group(1)!!
            }
            val uri = pathPattern.matcher(requestLine).let {
                if (!it.find() || it.groupCount() < 1) {
                    throw IllegalArgumentException("Unable to parse path from $requestLine")
                }
                it.group(1)!!.toUri()
            }

            // Compile request
            val request = HttpRequest(
                method = method,
                path = uri.path ?: "/",
                query = uri.queryParameterNames.map { it to uri.getQueryParameter(it)!! }.toMap(),
                headers = requestLines.drop(1).filter {
                    it.isNotEmpty()
                }.map {
                    val parts = it.split(":").map { it.trim() }
                    require(parts.size >= 2) { IllegalArgumentException("Splitting is less than 2 parts: '$it'") }
                    parts[0].uppercase() to parts.drop(1).joinToString(":")
                }.toMap(),
                body = null
            )

            // Read body
            val requestWithBody = request.headers["CONTENT-LENGTH"]?.toInt()?.let {
                Timber.i("Reading body ($it bytes)")
                val body = ByteArray(it)
                socket.getInputStream().read(body)
                request.copy(body = body)
            } ?: request

            Timber.i("Received request: $request (hasBody=${requestWithBody != request})")
            handleRequest(requestWithBody)
        } catch (e: Exception) {
            Timber.e(e)
            InternalErrorConnection(socket, executor)
        }
    }
}