package de.crysxd.octocam.server.http

import de.crysxd.octocam.BuildConfig
import de.crysxd.octocam.logging.TaggedTree
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import timber.log.Timber
import java.io.OutputStream
import java.io.OutputStreamWriter
import java.net.Socket
import java.util.concurrent.ExecutorService
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicBoolean

abstract class HttpConnection(
    val httpRequest: HttpRequest,
    private val socket: Socket,
    private val executor: ExecutorService,
    private val doOnClose: () -> Unit,
) {

    companion object {
        @JvmStatic
        protected val LINGER_TIMEOUT_MS = 2_000
        private const val CONNECT_TIMEOUT_MS = 2_000L
        private const val RESPONSE_TIMEOUT_MS = 2_000L
        const val NL = "\n"
        private var connectionCounter = 0
    }

    protected val connectionId = connectionCounter++
    val tag = "HttpConnection[${connectionId}]"
    protected val timber = TaggedTree(tag)
    protected lateinit var output: OutputStream
    private var wasClosed = AtomicBoolean(false)
    private val connectionJob = SupervisorJob()
    protected val connectionScope = CoroutineScope(connectionJob + Dispatchers.Main.immediate)
    val isActive get() = !wasClosed.get()

    fun sendResponse() {
        executor.execute {
            try {
                socket.soTimeout = CONNECT_TIMEOUT_MS.toInt()
                socket.setSoLinger(true, LINGER_TIMEOUT_MS)
                output = socket.getOutputStream().buffered()

                // Send response headers
                executor.submit {
                    val writer = output.writer()
                    if (writeResponse(writer)) {
                        timber.i("Response written, closing")
                        writer.flush()
                        close()
                    } else {
                        timber.i("Connection is requested to be kept open")
                        writer.flush()
                    }
                }.get(RESPONSE_TIMEOUT_MS, TimeUnit.MILLISECONDS)
            } catch (e: Exception) {
                Timber.e(e)
                InternalErrorConnection(socket, executor, doOnClose)
            }
        }
    }

    protected fun OutputStreamWriter.writeDefaultHeaders(code: Int) {
        timber.i("Sending $code")

        val message = when (code) {
            200 -> "OK"
            404 -> "Not Found"
            500 -> "Internal Server Error"
            503 -> "Service Unavailable"
            else -> "Internal Server Error"
        }
        write("HTTP/1.1 $code $message$NL")
        write("Cache-Control: no-store, no-cache, must-revalidate, pre-check=0, post-check=0, max-age=0$NL")
        write("Connection: close$NL")
        write("Expires: -1$NL")
        write("Access-Control-Allow-Origin: *$NL")
        write("Server: OctoCam;${BuildConfig.VERSION_NAME} (${BuildConfig.VERSION_CODE})$NL")
        write("Pragma: no-cache$NL")
    }

    protected fun OutputStreamWriter.writeContentHeaders(mimeType: String, length: Int) {
        write("Content-Length: $length$NL")
        write("Content-Type: $mimeType$NL")
    }

    protected fun OutputStreamWriter.closeHeaders() = write(NL)

    abstract fun writeResponse(writer: OutputStreamWriter): Boolean

    open fun close() {
        if (wasClosed.compareAndSet(false, true)) {
            timber.i("Closing")
            connectionJob.cancel()
            doOnClose()
            try {
                socket.close()
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }
}