package de.crysxd.octocam.server.http

import de.crysxd.octocam.repository.OctoCamSettingsRepository
import de.crysxd.octocam.server.service.MainServiceBinder
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import java.net.InetSocketAddress
import java.net.ServerSocket
import java.net.Socket
import java.util.concurrent.Executors
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicInteger

class HttpServer(
    val mainServiceBinder: MainServiceBinder,
    val port: Int = 5501,
    val onConnectionCountChanged: (Int) -> Unit = {},
) {

    private var isRunning = false
    private var serverSocket: ServerSocket = ServerSocket()
    private var atomicConnectionCount = AtomicInteger()
    private val executor = Executors.newCachedThreadPool()
    val connectionCount get() = atomicConnectionCount.get()

    fun start(scope: CoroutineScope) {
        if (isRunning) throw IllegalStateException("Already running")
        isRunning = true

        scope.launch(Dispatchers.IO) {
            Timber.i("Starting up")
            serverSocket = ServerSocket()
            serverSocket.reuseAddress = true
            serverSocket.bind(InetSocketAddress(port))
            Timber.i("Listening on port ${serverSocket.inetAddress.hostAddress}:$port")
            loop()
        }
    }

    fun stop() {
        Timber.i("Shutting down")
        serverSocket.close()
        isRunning = false
        Timber.i("Shut down and cleaned up")
    }

    private fun loop() {
        while (isRunning && !serverSocket.isClosed) {
            try {
                // Accept connection
                Timber.i("Waiting for connection")
                val socket = serverSocket.accept()

                // Read header and create connection
                Timber.i("New connection from ${socket.inetAddress.hostAddress}")
                HttpRequestReader(socket, executor) {
                    // Request read async, create connection to respond
                    createHttpConnection(socket, it)
                }
            } catch (e: Exception) {
                Timber.e(e, "Error while accepting connection")
            }
        }
    }

    private fun createHttpConnection(
        socket: Socket,
        request: HttpRequest,
    ) {
        Timber.i("Creating connection for ${request.path}")
        val connection = when (request.path) {
            "/" -> createStreamConnection(socket, request)
            "/torch" -> TorchControlConnection(socket, request, mainServiceBinder, executor)
            else -> ResourceNotFoundHttpConnection(request, socket, executor)
        }
        connection.sendResponse()
        Timber.i("Connection from ${socket.inetAddress.hostAddress} is handled by ${connection.tag} (${connection::class.java.simpleName})")
    }

    private fun createStreamConnection(socket: Socket, request: HttpRequest): HttpConnection {
        val reference = ConnectionReference()
        val doOnClose = {
            val count = reference.close()
            Timber.i("Connection from  ${socket.inetAddress.hostAddress} closed ($count active connections)")
            onConnectionCountChanged(count)
        }

        return try {
            // Check if we have capacity for stream
            val maxConnections = OctoCamSettingsRepository.value.maxConnections
            if (connectionCount > maxConnections) {
                // No capacity
                Timber.i("Connection from ${socket.inetAddress.hostAddress} exceeds limit of $maxConnections, closing...")
                ServerBusyHttpConnection(socket, executor, maxConnections, doOnClose)
            } else {
                // Capacity available
                Timber.i("Connection from ${socket.inetAddress.hostAddress} is within limit of $connectionCount / $maxConnections")
                onConnectionCountChanged(reference.open())
                MjpegStreamHttpConnection(request, socket, executor, doOnClose)
            }
        } catch (e: Exception) {
            doOnClose()
            Timber.e(e)
            InternalErrorConnection(socket, executor, doOnClose)
        }
    }

    private inner class ConnectionReference {
        val isClosed = AtomicBoolean(true)

        fun open() = if (isClosed.compareAndSet(true, false)) {
            atomicConnectionCount.incrementAndGet()
        } else {
            atomicConnectionCount.get()
        }

        fun close() = if (isClosed.compareAndSet(false, true)) {
            atomicConnectionCount.decrementAndGet()
        } else {
            atomicConnectionCount.get()
        }
    }
}
