package de.crysxd.octocam.server.http

import java.io.OutputStreamWriter
import java.net.Socket
import java.util.concurrent.ExecutorService

class ResourceNotFoundHttpConnection(
    httpRequest: HttpRequest,
    socket: Socket,
    executor: ExecutorService,
    doOnClose: () -> Unit = {},
) : HttpConnection(httpRequest, socket, executor, doOnClose) {

    override fun writeResponse(writer: OutputStreamWriter): Boolean {
        timber.i("Sending 404")
        val message =
            "<h1>Not Found</h1><p>Unable to find <b>${httpRequest.path}</b> on this server</p>"
        writer.writeDefaultHeaders(404)
        writer.writeContentHeaders("text/html", message.length)
        writer.closeHeaders()
        writer.write(message)
        return true
    }
}