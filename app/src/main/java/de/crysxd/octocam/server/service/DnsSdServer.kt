package de.crysxd.octocam.server.service

import android.content.Context
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import com.github.druk.dnssd.*
import timber.log.Timber


class DnsSdServer(
    private val context: Context,
    private val serviceName: String,
    private val serviceType: String = "_http._tcp",
    private val port: Int,
    private val onHostnameReady: (String) -> Unit,
    private val onError: () -> Unit
) {

    private val dnssd by lazy { DNSSDEmbedded(context) }
    private var service: DNSSDService? = null

    fun advertise(lifecycleOwner: LifecycleOwner) {
        lifecycleOwner.lifecycle.addObserver(object : LifecycleObserver {

            @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
            fun onCreate() = register()

            @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
            fun onDestroy() = unregister()

        })
    }

    private fun register() {
        // Ensure DNS service is running, Android may stop it
        context.getSystemService(Context.NSD_SERVICE)

        service = dnssd.register(
            serviceName,
            serviceType,
            port,
            object : RegisterListener {
                override fun serviceRegistered(
                    registration: DNSSDRegistration,
                    flags: Int,
                    serviceName: String,
                    regType: String,
                    domain: String
                ) {
                    Timber.i("DNS-SD service registered: serviceName=$serviceName regType=$regType domain=$domain")

                    dnssd.resolve(0, 0, serviceName, regType, domain, object : ResolveListener {
                        override fun operationFailed(service: DNSSDService, errorCode: Int) {
                            Timber.e("Unable to resolve own service")
                            onError()
                        }

                        override fun serviceResolved(
                            resolver: DNSSDService,
                            flags: Int,
                            ifIndex: Int,
                            fullName: String,
                            hostName: String,
                            port: Int,
                            txtRecord: MutableMap<String, String>
                        ) {
                            val cleanHost = hostName.removeSuffix(".").lowercase()
                            Timber.i("Resolved myself: $cleanHost")
                            onHostnameReady(cleanHost)
                        }
                    })
                }

                override fun operationFailed(service: DNSSDService?, errorCode: Int) {
                    Timber.e("DNS-SD operation failed: $errorCode")
                    onError()
                }
            })
    }

    private fun unregister() {
        service?.stop()
        Timber.i("DNS-SD service stopped")
    }
}