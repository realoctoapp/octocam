package de.crysxd.octocam.server.service

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.*
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.Preview
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.lifecycle.*
import de.crysxd.octocam.ext.allPermissionsGranted
import de.crysxd.octocam.ext.postStateUpdate
import de.crysxd.octocam.models.NetworkAddress
import de.crysxd.octocam.models.Resolution
import de.crysxd.octocam.models.ServiceState
import de.crysxd.octocam.repository.OctoCamSettingsRepository
import de.crysxd.octocam.server.camera.ImageMediator
import de.crysxd.octocam.server.http.HttpServer
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.net.NetworkInterface

@OptIn(ExperimentalAnimationApi::class)
class MainService : LifecycleService() {
    companion object {
        private const val NOTIFICATION_ID = 3
        var isRunning = false
            private set

        fun start(context: Context) {
            val intent = Intent(context, MainService::class.java)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(intent)
            } else {
                context.startService(intent)
            }
        }
    }


    // Camera
    private var previewUseCase: Preview? = null
    private var analyzeUseCase: ImageAnalysis? = null
    private var activeCameraId: Int = OctoCamSettingsRepository.value.cameraId
    private var activeResolution: Resolution = OctoCamSettingsRepository.value.streamResolution
    private val cameraManager = CameraManager(this, this) {
        stopSelf()
    }

    // Wake Lock
    private val wakeLock: PowerManager.WakeLock by lazy {
        (getSystemService(Context.POWER_SERVICE) as PowerManager).run {
            newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "OctoCam::Server")
        }
    }

    // Binder
    private val binder = object : MainServiceBinder() {
        override fun getServiceState() = mutableServiceState.map { it }

        override fun bindPreview(uc: Preview) {
            previewUseCase = uc
            startCameraAfterChecks()
        }

        override fun setLightOn(on: Boolean) {
            cameraManager.lightState = on
        }

        override fun unbindPreview() {
            previewUseCase = null
            startCameraAfterChecks()
        }

        override fun stopService() {
            stopSelf()
        }
    }

    // Server
    private val httpServer: HttpServer = HttpServer(binder) {
        Timber.i("Update connection count: $it")
        val uc = if (it > 0) analyzeUseCase ?: createAnalyzeUseCase() else null
        if (uc != analyzeUseCase) {
            analyzeUseCase = uc
            startCameraAfterChecks()
        }

        // Only touch light if automatic lights are enabled, pure UI control otherwise
        if (OctoCamSettingsRepository.value.automaticLights) {
            cameraManager.lightState = it > 0
        }

        // Update notification
        moveToForeground()

        // Update the live data
        handler.post {
            mutableServiceState.postStateUpdate { copy(connectionCount = it) }
        }
    }
    private val dnsSdServer = DnsSdServer(
        context = this,
        serviceName = OctoCamSettingsRepository.value.serviceName,
        port = httpServer.port,
        onHostnameReady = {
            lifecycleScope.launchWhenCreated {
                OctoCamSettingsRepository.update { copy(hostname = it) }
                val addresses = createAddresses()
                mutableServiceState.postStateUpdate { copy(addresses = addresses) }
            }
        },
        onError = {
            stopSelf()
        }
    )

    // State
    private val handler = Handler(Looper.getMainLooper())
    private val mutableServiceState = MediatorLiveData<ServiceState>()

    @SuppressLint("WakelockTimeout")
    override fun onCreate() {
        super.onCreate()
        Timber.i("Starting MainService")
        isRunning = true
        wakeLock.acquire()

        // Listen for settings changes
        OctoCamSettingsRepository.liveValue.observe(this, Observer {
            if (activeResolution != it.streamResolution || activeCameraId != it.cameraId) {
                Timber.i("Updating resolution and camera")
                activeResolution = it.streamResolution
                activeCameraId = it.cameraId
                analyzeUseCase = analyzeUseCase?.let { createAnalyzeUseCase() }
                startCameraAfterChecks()
            }
        })

        // Start up
        lifecycleScope.launchWhenCreated {
            moveToForeground()

            mutableServiceState.value = ServiceState(
                addresses = createAddresses(),
                port = httpServer.port,
                protocol = "http",
                connectionCount = 0,
                lightsOn = false
            )

            // Copy light status from camera manager
            mutableServiceState.addSource(cameraManager.liveLightState) {
                mutableServiceState.postValue(
                    mutableServiceState.value?.copy(
                        lightsOn = it
                    )
                )
            }

            httpServer.start(lifecycleScope)
            dnsSdServer.advertise(this@MainService)
            startCameraAfterChecks()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        httpServer.stop()
        wakeLock.release()
        isRunning = false
        Timber.i("Stopping MainService")
    }

    @Suppress("BlockingMethodInNonBlockingContext")
    private suspend fun createAddresses(): List<NetworkAddress> = withContext(Dispatchers.IO) {
        val ifs = NetworkInterface.getNetworkInterfaces().toList().map {
            it.inetAddresses.toList().filter { !it.isLoopbackAddress }
        }.flatten().map {
            listOfNotNull(it.hostName, it.hostAddress)
        }.flatten().distinct().filter {
            // Filter out addresses with : as we don't want IPv6
            !it.contains(':')
        }.map {
            NetworkAddress(quality = 5, address = it)
        }.toMutableList()

        OctoCamSettingsRepository.value.hostname?.let {
            ifs.add(
                NetworkAddress(quality = 10, address = it)
            )
        }

        ifs.toList()
    }

    private fun moveToForeground() {
        val notification = NotificationFactory(this).createServerActiveNotification(
            connectionCount = httpServer.connectionCount
        )
        startForeground(NOTIFICATION_ID, notification)
    }

    private fun createAnalyzeUseCase() = ImageAnalysis.Builder()
        .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
        .setTargetResolution(activeResolution.size(OctoCamSettingsRepository.value.sensorRotationDegrees))
        .build()
        .also {
            it.setAnalyzer(ImageMediator.executor, ImageMediator)
        }

    private fun startCameraAfterChecks() {
        if (allPermissionsGranted()) {
            Timber.i("Starting camera after permission check")
            cameraManager.updateCamera(previewUseCase, analyzeUseCase)
        } else {
            Timber.i("Missing permissions, stopping self")
            stopSelf()
        }
    }

    override fun onBind(intent: Intent): IBinder {
        super.onBind(intent)
        return binder
    }
}