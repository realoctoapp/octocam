package de.crysxd.octocam.models

import android.util.Size
import kotlin.math.absoluteValue

fun Size.differenceTo(other: Size) =
    (height - other.height).absoluteValue + (width - other.width).absoluteValue