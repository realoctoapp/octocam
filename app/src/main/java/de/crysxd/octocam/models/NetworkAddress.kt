package de.crysxd.octocam.models

data class NetworkAddress(
    val quality: Int,
    val address: String,
)