package de.crysxd.octocam.models

data class OctoCamSettings(
    val streamResolution: Resolution = Resolution.Medium,
    val maxConnections: Int = 10,
    val cameraId: Int = 0,
    val sensorRotationDegrees: Int = 0,
    val hardwareCameraId: String? = null,
    val targetFps: Int = 15,
    val startAfterBoot: Boolean = false,
    val automaticLights: Boolean = false,
    val lowBatteryWarning: Boolean = false,
    val batteryReminder: Boolean = false,
    val useWebP: Boolean = false,
    val statsForNerds: Boolean = false,
    val lastImageWidth: Int = 3,
    val lastImageHeight: Int = 4,
    val serviceName: String = String.format("octocam-%x", (0..4095).random()),
    val hostname: String? = null
)
