package de.crysxd.octocam.models

data class ServiceState(
    val addresses: List<NetworkAddress>,
    val port: Int,
    val protocol: String,
    val connectionCount: Int,
    val lightsOn: Boolean?,
)