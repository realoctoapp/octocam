package de.crysxd.octocam.models

import android.util.Size
import android.view.Surface

enum class Resolution {
    Low, Medium, High, Ultra;

    fun size(orientation: Int) = when (this) {
        Low -> Size(320, 240)
        Medium -> Size(640, 480)
        High -> Size(1280, 960)
        Ultra -> Size(1600, 1200)
    }.let {
        if (orientation == Surface.ROTATION_90 || orientation == Surface.ROTATION_270) {
            it
        } else {
            Size(it.height, it.width)
        }
    }

    val label
        get() = when (this) {
            Low -> "Low"
            Medium -> "Medium"
            High -> "High"
            Ultra -> "Ultra"
        }

    val next get() = values()[(values().indexOf(this) + 1) % values().size]
}