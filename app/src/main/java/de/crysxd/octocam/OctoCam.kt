package de.crysxd.octocam

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.provider.Settings
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.auth.ktx.auth
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfigSettings
import de.crysxd.octocam.logging.TimberCacheTree
import de.crysxd.octocam.repository.OctoCamSettingsRepository
import timber.log.Timber

class OctoCam : Application() {
    init {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
            Timber.plant(TimberCacheTree)
        }
    }

    override fun onCreate() {
        super.onCreate()

        // Setup settings
        OctoCamSettingsRepository.init(this)

        // Setup boot receiver
        registerReceiver(BootCompletedReceiver(), IntentFilter(Intent.ACTION_BOOT_COMPLETED))

        // Register default notification channel
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            manager.createNotificationChannel(
                NotificationChannel(
                    getString(R.string.updates_notification_channel),
                    getString(R.string.updates_notification_channel_name),
                    NotificationManager.IMPORTANCE_HIGH
                )
            )
        }

        // Setup RemoteConfig
        Firebase.remoteConfig.setDefaultsAsync(R.xml.remote_config_defaults)
        Firebase.remoteConfig.setConfigSettingsAsync(remoteConfigSettings {
            minimumFetchIntervalInSeconds = if (BuildConfig.DEBUG) 10 else 3600
        })
        Firebase.remoteConfig.fetchAndActivate().addOnCompleteListener {
            it.exception?.let(Timber::e)
            Timber.i("Complete remote config fetch (success=${it.isSuccessful})")
        }

        // Setup FCM
        FirebaseMessaging.getInstance().token.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                Timber.tag("FCM").i("Token: ${task.result}")
            } else {
                Timber.tag("FCM").w("Unable to get token")
            }
        }

        // Create anonymous user
        if (Firebase.auth.currentUser == null) {
            Firebase.auth.signInAnonymously().addOnSuccessListener {
                Timber.i("Signed in anonymously as ${it.user?.uid}")
                FirebaseCrashlytics.getInstance()
                    .setCustomKey("User ID", Firebase.auth.currentUser?.uid ?: "???")
            }.addOnFailureListener {
                Timber.e("Failed to sign in: $it")
            }
        } else {
            Timber.i("Already signed in as ${Firebase.auth.currentUser?.uid}")
            FirebaseCrashlytics.getInstance()
                .setCustomKey("User ID", Firebase.auth.currentUser?.uid ?: "???")
        }

        // Setup analytics
        // Do not enable if we are in a TestLab environment
        FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true)
        val isTestLab = Settings.System.getString(contentResolver, "firebase.test.lab") == "true"
        Firebase.analytics.setAnalyticsCollectionEnabled(!isTestLab && BuildConfig.DEBUG)
        if (BuildConfig.DEBUG) {
            Firebase.analytics.setUserProperty("debug", "true")
        }
    }
}