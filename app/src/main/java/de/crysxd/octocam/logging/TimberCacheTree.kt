package de.crysxd.octocam.logging

import android.util.Log
import timber.log.Timber
import java.io.PrintWriter
import java.io.StringWriter
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

object TimberCacheTree : Timber.DebugTree() {
    private val maxSize: Int = 1024 * 128
    private val maxMessageLength = 4000
    private val dateFormat = SimpleDateFormat("yyyy/MM/dd hh:mm:ss.SSS", Locale.ENGLISH)
    private val cache = StringBuilder()
    private val lock = ReentrantLock()

    val logs get() = cache.toString()

    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        if (priority > Log.VERBOSE) {
            lock.withLock {
                val prefix = "${getTime()} ${getLevel(priority)}/${tag ?: "???"}: "
                cache.append(prefix)
                cache.append(message.take(maxMessageLength))
                cache.append("\n")

                t?.let {
                    val stackTrace = StringWriter()
                    val writer = PrintWriter(stackTrace)
                    t.printStackTrace(writer)

                    stackTrace.buffer.lines().forEach {
                        cache.append(prefix)
                        cache.append(it)
                        cache.append("\n")
                    }
                }

                if (cache.length > maxSize) {
                    cache.delete(0, cache.length - maxSize)
                }
            }
        }
    }

    private fun getTime() = dateFormat.format(Date())

    private fun getLevel(priority: Int) = when (priority) {
        Log.VERBOSE -> "V"
        Log.DEBUG -> "D"
        Log.INFO -> "I"
        Log.WARN -> "W"
        Log.ERROR -> "E"
        Log.ASSERT -> "A"
        else -> "?"
    }
}