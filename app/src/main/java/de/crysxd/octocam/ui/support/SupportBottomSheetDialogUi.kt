package de.crysxd.octocam.ui.support

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import de.crysxd.octocam.ui.base.Buttons
import de.crysxd.octocam.ui.base.Titles
import de.crysxd.octocam.ui.theme.*

class SupportBottomSheetDialogUi(
    private val send: (String, Boolean, Boolean) -> Unit
) {

    @Composable
    fun Layout() {
        Column(
            modifier = Modifier.padding(ScreenMargins)
        ) {
            val message = remember { mutableStateOf("") }
            val includeLogs = remember { mutableStateOf(true) }
            val includePhoneInfo = remember { mutableStateOf(true) }
            val showDialog = remember { mutableStateOf(false) }

            Titles.SectionTitle(
                title = "Get in touch",
                modifier = Modifier.padding(bottom = Margin20)
            )
            Text(
                text = "Do you need help or have feedback? Drop me a message!",
                textAlign = TextAlign.Center,
                modifier = Modifier.padding(bottom = Margin30)
            )
            OutlinedTextField(
                value = message.value,
                shape = MaterialTheme.shapes.medium,
                label = {
                    Text("Message")
                },
                onValueChange = { txt ->
                    message.value = txt
                },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = Margin30)
            )
            CheckboxWithLabel(
                state = includeLogs,
                label = "Include app logs (required for bugs)",
                modifier = Modifier.padding(bottom = Margin0),
            )
            CheckboxWithLabel(
                state = includePhoneInfo,
                label = "Phone information (required for bugs)",
            )
            Buttons.PrimaryButton(
                text = "Open Email",
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = Margin30)
            ) {
                if (message.value.isEmpty()) {
                    showDialog.value = true
                } else {
                    send(
                        message.value,
                        includeLogs.value,
                        includePhoneInfo.value,
                    )
                }
            }

            if (showDialog.value) {
                AlertDialog(
                    text = {
                        Text(
                            text = "Please enter a message",
                            style = Typography.body1
                        )
                    },
                    onDismissRequest = { showDialog.value = false },
                    confirmButton = {
                        Buttons.PrimaryButtonSmall(
                            text = "OK",
                            onClick = { showDialog.value = false }
                        )
                    }
                )
            }
        }
    }

    @Composable
    fun CheckboxWithLabel(
        state: MutableState<Boolean>,
        label: String,
        modifier: Modifier = Modifier
    ) {
        Row(
            modifier = modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Checkbox(
                checked = state.value,
                onCheckedChange = { s ->
                    state.value = s
                },
                modifier = Modifier
            )
            Text(text = label, modifier = Modifier.padding(start = Margin5))
        }
    }
}

@Preview
@Composable
fun SupportPreview() {
    OctoCamTheme {
        SupportBottomSheetDialogUi({ _, _, _ -> }).Layout()
    }
}