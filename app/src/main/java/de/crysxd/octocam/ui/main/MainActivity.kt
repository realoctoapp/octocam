package de.crysxd.octocam.ui.main

import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.view.WindowManager
import android.widget.Toast
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.core.app.ActivityCompat
import androidx.lifecycle.ViewModelProvider
import de.crysxd.octocam.ext.allPermissionsGranted
import de.crysxd.octocam.ext.requiredPermissions
import de.crysxd.octocam.server.service.MainService
import de.crysxd.octocam.server.service.MainServiceBinder
import de.crysxd.octocam.ui.instructions.InstructionsBottomSheetDialog
import de.crysxd.octocam.ui.support.SupportBottomSheetDialog
import de.crysxd.octocam.ui.theme.OctoCamTheme
import timber.log.Timber

@ExperimentalAnimationApi
class MainActivity : AppCompatActivity() {
    companion object {
        private const val REQUEST_CODE_PERMISSIONS = 10
    }

    private var service: MainServiceBinder? = null
    private val ui = MainActivityUi(
        startStream = ::startAndBindService,
        stopStream = ::stopAndUnbindService,
        showInstructions = ::showInstructions,
        sendFeedback = ::sendFeedback,
        setLightOn = ::setLightOn
    )
    private val viewModel by lazy {
        ViewModelProvider(this)[MainActivityViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        setContent {
            OctoCamTheme {
                ui.Screen(viewModel)
            }
        }
    }

    override fun onStart() {
        super.onStart()
        startAndBindService()
    }

    private fun setLightOn(on: Boolean) {
        service?.setLightOn(on)
    }

    private fun startAndBindService() {
        if (allPermissionsGranted()) {
            Timber.i("Connecting service")
            MainService.start(this)
            bindService(Intent(this, MainService::class.java), connection, 0)
        } else {
            ActivityCompat.requestPermissions(this, requiredPermissions, REQUEST_CODE_PERMISSIONS)
        }
    }

    private fun stopAndUnbindService() {
        unbindService()
        service?.stopService()
    }

    private fun showInstructions() {
        InstructionsBottomSheetDialog().show(supportFragmentManager, "instructions")
    }

    private fun sendFeedback() {
        SupportBottomSheetDialog().show(supportFragmentManager, "help")
    }

    private fun unbindService() {
        try {
            service?.unbindPreview()
            viewModel.notifyServiceStopped()
            unbindService(connection)
        } catch (e: Exception) {
            Timber.w("Failed to unbind service cleanly")
        }
    }

    override fun onStop() {
        super.onStop()
        unbindService()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                startAndBindService()
            } else {
                Toast.makeText(
                    this,
                    "Permissions not granted by the user.",
                    Toast.LENGTH_SHORT
                ).show()
                finish()
            }
        }
    }

    /** Defines callbacks for service binding, passed to bindService()  */
    private val connection = object : ServiceConnection {

        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            Timber.i("Service connected")
            // We've bound to MainService, cast the IBinder and get LocalService instance
            this@MainActivity.service = service as MainServiceBinder

            // Setup preview
            ui.preview.observe(this@MainActivity) {
                this@MainActivity.service?.bindPreview(it)
            }

            // Report to VM so view updates
            viewModel.notifyServiceStarted()
            this@MainActivity.service?.getServiceState()?.let {
                viewModel.registerServiceState(it)
            }
        }

        override fun onServiceDisconnected(arg: ComponentName) {
            Timber.i("Service disconnected")
            viewModel.notifyServiceStopped()
            service = null
        }
    }
}