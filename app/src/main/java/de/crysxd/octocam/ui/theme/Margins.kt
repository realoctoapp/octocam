package de.crysxd.octocam.ui.theme

import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

val Margin0 = 2.dp
val Margin5 = 5.dp
val Margin10 = 10.dp
val Margin20 = 20.dp
val Margin30 = 30.dp
val Margin60 = 60.dp

val ScreenMargins: Dp
    @Composable get() {
        val width = LocalConfiguration.current.screenWidthDp
        return if (width < 330) {
            Margin10
        } else {
            Margin20
        }
    }
