package de.crysxd.octocam.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import de.crysxd.octocam.ext.postStateUpdate
import de.crysxd.octocam.models.OctoCamSettings
import de.crysxd.octocam.models.ServiceState
import de.crysxd.octocam.repository.OctoCamSettingsRepository
import timber.log.Timber

class MainActivityViewModel : ViewModel() {
    private val mutableState = MediatorLiveData<UiState>().also {
        it.value = UiState()
    }
    val state = mutableState.map { it }

    init {
        mutableState.addSource(OctoCamSettingsRepository.liveValue) {
            mutableState.postStateUpdate {
                copy(settings = it)
            }
        }
    }

    fun registerServiceState(liveData: LiveData<ServiceState>) {
        mutableState.addSource(liveData) {
            mutableState.postStateUpdate {
                Timber.i("Service state: $it")
                copy(serviceState = it)
            }
        }
    }

    fun notifyServiceStopped() = mutableState.postStateUpdate {
        copy(streamActive = false)
    }

    fun notifyServiceStarted() = mutableState.postStateUpdate {
        copy(streamActive = true)
    }

    data class UiState(
        val streamActive: Boolean = false,
        val settings: OctoCamSettings = OctoCamSettingsRepository.value,
        val serviceState: ServiceState? = null,
    )
}