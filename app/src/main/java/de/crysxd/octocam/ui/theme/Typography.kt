package de.crysxd.octocam.ui.theme

import androidx.compose.material.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import de.crysxd.octocam.R

// Set of Material typography styles to start with
private val RobotoLight = Font(R.font.roboto_light, FontWeight.Light, FontStyle.Normal)
private val RobotoRegular = Font(R.font.roboto_regular, FontWeight.Normal, FontStyle.Normal)
private val RobotoMedium = Font(R.font.roboto_medium, FontWeight.Medium, FontStyle.Normal)
private val Roboto = FontFamily(RobotoLight, RobotoMedium, RobotoRegular)
private val UbuntuLight = Font(R.font.ubuntu_light, FontWeight.Light, FontStyle.Normal)
private val UbuntuRegular = Font(R.font.ubuntu_regular, FontWeight.Normal, FontStyle.Normal)
private val Ubuntu = FontFamily(UbuntuLight, UbuntuRegular)

val Typography = Typography(
    body1 = TextStyle(
        fontFamily = Roboto,
        fontWeight = FontWeight.Normal,
        fontSize = 14.sp
    ),
    h1 = TextStyle(
        fontFamily = Ubuntu,
        fontWeight = FontWeight.Normal,
        fontSize = 40.sp
    ),
    h2 = TextStyle(
        fontFamily = Ubuntu,
        fontWeight = FontWeight.Light,
        fontSize = 28.sp,
    ),
    h3 = TextStyle(
        fontFamily = Ubuntu,
        fontWeight = FontWeight.Light,
        fontSize = 22.sp,
    ),
    button = TextStyle(
        fontFamily = Ubuntu,
        fontWeight = FontWeight.Light,
        fontSize = 20.sp
    ),
    caption = TextStyle(
        fontFamily = Roboto,
        fontWeight = FontWeight.Medium,
        fontSize = 16.sp
    ),
)