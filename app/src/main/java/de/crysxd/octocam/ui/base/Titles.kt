package de.crysxd.octocam.ui.base

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import de.crysxd.octocam.ui.theme.OctoCamTheme
import de.crysxd.octocam.ui.theme.Typography
import de.crysxd.octocam.ui.theme.darkText
import de.crysxd.octocam.ui.theme.lightText

object Titles {

    @Composable
    fun SectionTitle(
        title: String,
        modifier: Modifier = Modifier,
        prefix: String? = null,
        suffix: String? = null
    ) {
        Text(
            modifier = modifier.fillMaxWidth(),
            style = Typography.h3,
            color = MaterialTheme.colors.darkText,
            textAlign = TextAlign.Center,
            text = buildAnnotatedString {
                prefix?.let {
                    withStyle(SpanStyle(color = MaterialTheme.colors.lightText)) {
                        append(it)
                    }
                }
                append(title)
                suffix?.let {
                    withStyle(SpanStyle(color = MaterialTheme.colors.lightText)) {
                        append(it)
                    }
                }
            },
            overflow = TextOverflow.Visible
        )
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewSectionTitle1() {
    OctoCamTheme {
        Titles.SectionTitle(title = "This is a title")
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewSectionTitle2() {
    OctoCamTheme {
        Titles.SectionTitle(prefix = "http://", title = "octocam-3af.local")
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewSectionTitle3() {
    OctoCamTheme {
        Titles.SectionTitle(prefix = "http://", title = "octocam.local", suffix = ":5501/")
    }
}