package de.crysxd.octocam.ui.main

import android.content.Context
import android.hardware.camera2.CameraManager
import androidx.camera.view.PreviewView
import androidx.compose.animation.*
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import de.crysxd.octocam.R
import de.crysxd.octocam.models.NetworkAddress
import de.crysxd.octocam.models.OctoCamSettings
import de.crysxd.octocam.models.ServiceState
import de.crysxd.octocam.repository.OctoCamSettingsRepository
import de.crysxd.octocam.ui.base.Buttons
import de.crysxd.octocam.ui.base.Titles
import de.crysxd.octocam.ui.theme.*

@ExperimentalAnimationApi
class MainActivityUi(
    private val startStream: () -> Unit = {},
    private val stopStream: () -> Unit = {},
    private val showInstructions: () -> Unit = {},
    private val sendFeedback: () -> Unit = {},
    private val setLightOn: (Boolean) -> Unit = {},
) {

    private val mutablePreview = MutableLiveData<androidx.camera.core.Preview>()
    val preview = mutablePreview as LiveData<androidx.camera.core.Preview>

    @Composable
    fun Screen(viewModel: MainActivityViewModel) {
        val s by viewModel.state.observeAsState(MainActivityViewModel.UiState())
        Layout(uiState = s)
    }

    @Composable
    fun Layout(uiState: MainActivityViewModel.UiState) {
        Column(
            modifier = Modifier
                .verticalScroll(rememberScrollState())
                .padding(ScreenMargins),
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            Header(uiState = uiState, modifier = Modifier.padding(bottom = Margin30))
            Settings(settings = uiState.settings)
        }
    }

    @Composable
    fun Header(
        uiState: MainActivityViewModel.UiState,
        modifier: Modifier = Modifier
    ) = Column(
        modifier = modifier,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        val streamModifier = Modifier
            .fillMaxWidth()
            .clip(Shapes.large)
            .background(Color.Black)
            .animateContentSize()
            .aspectRatio(uiState.settings.lastImageHeight / uiState.settings.lastImageWidth.toFloat())

        // Title
        AnimatedContent(
            targetState = uiState.streamActive,
            transitionSpec = {
                // Compare the incoming number with the previous number.
                if (targetState) {
                    // If the target number is larger, it slides up and fades in
                    // while the initial (smaller) number slides up and fades out.
                    slideInVertically({ height -> height }) +
                            fadeIn() with slideOutVertically({ height -> -height }) +
                            fadeOut()
                } else {
                    // If the target number is smaller, it slides down and fades in
                    // while the initial number slides down and fades out.
                    slideInVertically({ height -> -height }) +
                            fadeIn() with slideOutVertically({ height -> height }) +
                            fadeOut()
                }.using(
                    // Disable clipping since the faded slide-in/out should
                    // be displayed out of bounds.
                    SizeTransform(clip = false)
                )
            }
        ) { streamActive ->
            Column {
                Titles.SectionTitle(
                    prefix = if (streamActive) "${uiState.serviceState?.protocol}://" else null,
                    title = if (streamActive) {
                        uiState.serviceState?.addresses?.maxByOrNull { it.quality }?.address ?: ""
                    } else {
                        "Welcome to OctoCam!"
                    },
                    suffix = if (streamActive) ":${uiState.serviceState?.port}/" else null,
                    modifier = Modifier
                        .padding(bottom = Margin5, top = Margin60)
                        .fillMaxWidth()
                        .horizontalScroll(rememberScrollState())
                )
                Text(
                    text = buildAnnotatedString {
                        if (streamActive) {
                            val text =
                                "Use the above as stream URL in your OctoPrint settings or [see alternative addresses and full instructions]"
                            val start = text.indexOf('[')
                            val end = text.indexOf(']') - 1
                            append(text.replace("[", "").replace("]", ""))
                            addStyle(
                                style = SpanStyle(
                                    color = MaterialTheme.colors.secondary,
                                    textDecoration = TextDecoration.Underline
                                ),
                                start = start,
                                end = end
                            )
                        } else {
                            append("Start the camera to get begin")
                        }
                    },
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(bottom = Margin30)
                        .clickable {
                            showInstructions()
                        },
                    color = MaterialTheme.colors.normalText,
                )
            }
        }

        // Preview
        Box(
            contentAlignment = Alignment.BottomCenter
        ) {
            StreamPreview(modifier = streamModifier, uiState = uiState)
            Buttons.PrimaryButtonSmall(
                text = if (uiState.streamActive) "Stop camera" else "Start camera",
                onClick = if (uiState.streamActive) stopStream else startStream,
                modifier = Modifier.padding(bottom = Margin10)
            )
        }
    }

    @Composable
    fun Settings(settings: OctoCamSettings) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            Titles.SectionTitle(
                title = "Settings",
                modifier = Modifier.padding(bottom = Margin10)
            )
            ResolutionSetting(settings = settings)
            TargetFpsSetting(settings = settings)
            ConnectionCountSetting(settings = settings)
            CameraSelectionSetting(settings = settings)
            StartAfterRebootSetting(settings = settings)
            AutomaticLightsSetting(settings = settings)
            SendFeedback()

            Box(modifier = Modifier.padding(top = Margin30))
            Titles.SectionTitle(
                title = "Coming soon...",
                modifier = Modifier.padding(bottom = Margin10)
            )
            Buttons.SettingsButton(
                text = "Low battery warning",
                iconPainter = painterResource(id = R.drawable.ic_round_battery_alert_24),
                switchState = true,
                isLocked = true
            ) {
            }
            Buttons.SettingsButton(
                text = "Battery reminder",
                iconPainter = painterResource(id = R.drawable.ic_round_battery_charging_full_24),
                rightDetail = "Monthly",
                isLocked = true
            ) {
            }
            Buttons.SettingsButton(
                text = "Use WebP encoding",
                iconPainter = painterResource(id = R.drawable.ic_round_animation_24),
                rightDetail = "Monthly",
                isLocked = true
            ) {
            }
            Buttons.SettingsButton(
                text = "Stats for nerds",
                iconPainter = painterResource(id = R.drawable.ic_round_bar_chart_24),
                rightDetail = "Monthly",
                isLocked = true
            ) {
            }
        }
    }

    @Composable
    fun ResolutionSetting(settings: OctoCamSettings) {
        Buttons.SettingsButton(
            text = "Stream resolution",
            iconPainter = painterResource(id = R.drawable.ic_round_photo_size_select_large_24),
            rightDetail = settings.streamResolution.label
        ) {
            OctoCamSettingsRepository.update { copy(streamResolution = settings.streamResolution.next) }
        }
    }

    @Composable
    fun ConnectionCountSetting(settings: OctoCamSettings) {
        val infinite = Int.MAX_VALUE
        val count = settings.maxConnections

        Buttons.SettingsButton(
            text = "Max connections",
            iconPainter = painterResource(id = R.drawable.ic_round_network_check_24),
            rightDetail = if (count == infinite) "∞" else count.toString(),
        ) {
            val maxConnectionOptions = listOf(1, 3, 5, 10, 20, infinite)
            val nextOption = maxConnectionOptions.firstOrNull { it > settings.maxConnections }
                ?: maxConnectionOptions.first()
            OctoCamSettingsRepository.update { copy(maxConnections = nextOption) }
        }
    }

    @Composable
    fun TargetFpsSetting(settings: OctoCamSettings) {
        val infinite = Int.MAX_VALUE
        val fps = settings.targetFps

        Buttons.SettingsButton(
            text = "Target frame rate",
            iconPainter = painterResource(id = R.drawable.ic_round_speed_24),
            rightDetail = if (fps == infinite) "∞" else "$fps FPS",
        ) {
            val maxConnectionOptions = listOf(5, 10, 15, 20, 25, 30, infinite)
            val nextOption = maxConnectionOptions.firstOrNull { it > settings.targetFps }
                ?: maxConnectionOptions.first()
            OctoCamSettingsRepository.update { copy(targetFps = nextOption) }
        }
    }

    @Composable
    fun CameraSelectionSetting(settings: OctoCamSettings) {
        val manager = LocalContext.current.getSystemService(Context.CAMERA_SERVICE) as CameraManager
        val count = manager.cameraIdList.size
        Buttons.SettingsButton(
            text = "Camera",
            iconPainter = painterResource(id = R.drawable.ic_round_videocam_24),
            rightDetail = "Camera #${settings.cameraId + 1}",
        ) {
            OctoCamSettingsRepository.update {
                copy(cameraId = (settings.cameraId + 1) % count)
            }
        }
    }

    @Composable
    fun StartAfterRebootSetting(settings: OctoCamSettings) {
        Buttons.SettingsButton(
            text = "Start after boot",
            iconPainter = painterResource(id = R.drawable.ic_round_settings_backup_restore_24),
            switchState = settings.startAfterBoot,
        ) {
            OctoCamSettingsRepository.update { copy(startAfterBoot = !settings.startAfterBoot) }
        }
    }

    @Composable
    fun AutomaticLightsSetting(settings: OctoCamSettings) {
        Buttons.SettingsButton(
            text = "Automatic lights",
            iconPainter = painterResource(id = R.drawable.ic_round_lightbulb_24),
            switchState = settings.automaticLights,
        ) {
            OctoCamSettingsRepository.update { copy(automaticLights = !settings.automaticLights) }
        }
    }

    @Composable
    fun SendFeedback() {
        Buttons.SettingsButton(
            text = "Send Feedback",
            iconPainter = painterResource(id = R.drawable.ic_round_feedback_24),
        ) {
            sendFeedback()
        }
    }

    @Composable
    fun LightButton(serviceState: ServiceState?, modifier: Modifier = Modifier) {
        serviceState?.lightsOn?.let {
            IconButton(
                modifier = modifier
                    .width(40.dp)
                    .clip(CircleShape)
                    .background(MaterialTheme.colors.imageOverlay)
                    .padding(Margin10)
                    .aspectRatio(1f),
                onClick = {
                    setLightOn(!it)
                },
            ) {
                Icon(
                    painter = painterResource(id = if (it) R.drawable.ic_round_flashlight_off_24 else R.drawable.ic_round_flashlight_on_24),
                    contentDescription = if (it) "Turn light off" else "Turn light on",
                    tint = MaterialTheme.colors.darkText
                )
            }
        }
    }

    @Composable
    fun StreamPreview(modifier: Modifier = Modifier, uiState: MainActivityViewModel.UiState) =
        Box(modifier) {
            Crossfade(targetState = uiState.streamActive) { active ->
                if (active) {
                    Box(
                        contentAlignment = Alignment.TopEnd,
                    ) {
                        AndroidView(
                            factory = { ctx ->
                                val previewView = PreviewView(ctx)
                                mutablePreview.postValue(
                                    androidx.camera.core.Preview.Builder().build().also {
                                        it.setSurfaceProvider(previewView.surfaceProvider)
                                    }
                                )
                                previewView
                            },
                            modifier = Modifier
                                .fillMaxWidth()
                                .fillMaxHeight()
                        )
                        Live(
                            connectionCount = uiState.serviceState?.connectionCount ?: 0,
                            modifier = Modifier.padding(all = Margin10)
                        )
                    }
                    Box(
                        contentAlignment = Alignment.TopStart,
                    ) {
                        LightButton(
                            serviceState = uiState.serviceState,
                            modifier = Modifier.padding(all = Margin10)
                        )
                    }
                }
            }
        }

    @Composable
    fun Live(connectionCount: Int, modifier: Modifier = Modifier) {
        Box(modifier = modifier) {
            Text(
                text = if (connectionCount == 0) "No connections" else "$connectionCount connections",
                color = MaterialTheme.colors.inverseText,
                modifier = Modifier
                    .clip(Shapes.small)
                    .background(MaterialTheme.colors.liveBadge)
                    .padding(vertical = Margin0, horizontal = Margin10),
            )
        }
    }
}

@Composable
@ExperimentalAnimationApi
@Preview(showBackground = true)
fun Preview_MainActivity_StreamStopped() {
    OctoCamTheme {
        MainActivityUi().Layout(
            MainActivityViewModel.UiState(
                serviceState = ServiceState(
                    addresses = listOf(
                        NetworkAddress(10, "octocam-3fd.local")
                    ),
                    protocol = "http",
                    port = 5501,
                    connectionCount = 2,
                    lightsOn = true
                ),
                streamActive = true,
                settings = OctoCamSettings()
            )
        )
    }
}

@Composable
@ExperimentalAnimationApi
@Preview(showBackground = true)
fun Preview_MainActivity_StreamStarted() {
    OctoCamTheme {
        MainActivityUi().Layout(
            MainActivityViewModel.UiState(
                streamActive = false,
                settings = OctoCamSettings()
            )
        )
    }
}