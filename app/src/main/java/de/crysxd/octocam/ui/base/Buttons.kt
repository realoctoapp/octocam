package de.crysxd.octocam.ui.base

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import de.crysxd.octocam.R
import de.crysxd.octocam.ui.base.Buttons.PrimaryButton
import de.crysxd.octocam.ui.base.Buttons.PrimaryButtonSmall
import de.crysxd.octocam.ui.base.Buttons.SettingsButton
import de.crysxd.octocam.ui.theme.*

object Buttons {

    @Composable
    fun PrimaryButton(
        text: String,
        modifier: Modifier = Modifier,
        onClick: () -> Unit
    ) {
        Button(
            onClick = onClick,
            colors = ButtonDefaults.buttonColors(
                backgroundColor = MaterialTheme.colors.secondary,
                contentColor = MaterialTheme.colors.inverseText
            ),
            modifier = modifier.clip(Shapes.small)

        ) {
            Text(
                text = text,
                modifier = Modifier.padding(horizontal = Margin20, vertical = Margin10)
            )
        }
    }

    @Composable
    fun Link(
        text: String,
        modifier: Modifier = Modifier,
        onClick: () -> Unit
    ) {
        Text(
            text = text,
            modifier = modifier
                .clickable { onClick() }
                .padding(Margin5),
            color = MaterialTheme.colors.secondary,
            style = TextStyle(textDecoration = TextDecoration.Underline),
            textAlign = TextAlign.Center,
        )
    }

    @Composable
    fun PrimaryButtonSmall(
        text: String,
        modifier: Modifier = Modifier,
        onClick: () -> Unit
    ) {
        Button(
            onClick = onClick,
            colors = ButtonDefaults.buttonColors(
                backgroundColor = MaterialTheme.colors.secondary,
                contentColor = MaterialTheme.colors.inverseText
            ),
            modifier = modifier.clip(Shapes.small)
        ) {
            Text(
                text = text,
            )
        }
    }

    @Composable
    fun SettingsButton(
        text: String,
        iconPainter: Painter,
        rightDetail: String? = null,
        switchState: Boolean? = null,
        isLocked: Boolean = false,
        isSupportButton: Boolean = false,
        onClick: () -> Unit
    ) {
        val foreground = if (isSupportButton) {
            MaterialTheme.colors.supportButtonForeground
        } else {
            MaterialTheme.colors.settingsButtonForeground
        }
        val background = if (isSupportButton) {
            MaterialTheme.colors.supportButtonBackground
        } else {
            MaterialTheme.colors.settingsButtonBackground
        }

        Button(
            onClick = onClick,
            colors = ButtonDefaults.buttonColors(
                backgroundColor = background,
                contentColor = MaterialTheme.colors.inverseText
            ),
            modifier = Modifier
                .fillMaxWidth()
                .clip(Shapes.small)
                .padding(bottom = Margin5),
            elevation = null,

            ) {
            Row(verticalAlignment = Alignment.CenterVertically) {
                Icon(
                    painter = iconPainter,
                    tint = foreground,
                    contentDescription = null,
                    modifier = Modifier.padding(end = Margin10)
                )
                Text(
                    text = text,
                    color = MaterialTheme.colors.darkText,
                    overflow = TextOverflow.Ellipsis,
                    softWrap = false,
                    modifier = Modifier
                        .padding(end = Margin5)
                        .weight(1f)
                )

                if (!isLocked) {
                    rightDetail?.let {
                        Text(
                            text = rightDetail,
                            style = Typography.caption,
                            color = MaterialTheme.colors.normalText,
                        )
                    }
                    switchState?.let {
                        Switch(
                            checked = it,
                            onCheckedChange = { onClick() },
                            colors = SwitchDefaults.colors(
                                checkedThumbColor = foreground,
                                uncheckedThumbColor = MaterialTheme.colors.lightText,
                                uncheckedTrackAlpha = 0.2f
                            )
                        )
                    }
                } else {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_round_lock_24),
                        tint = MaterialTheme.colors.lightText,
                        contentDescription = null,
                    )
                }
            }
        }
    }
}

@Preview(showBackground = false)
@Composable
fun PreviewPrimaryButton() {
    OctoCamTheme {
        PrimaryButton("Test") {}
    }
}


@Preview(showBackground = false)
@Composable
fun PreviewPrimaryButtonSmall() {
    OctoCamTheme {
        PrimaryButtonSmall("Test") {}
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewSettingsButton() {
    OctoCamTheme {
        Column {
            SettingsButton(
                text = "Unlock all features",
                switchState = false,
                iconPainter = painterResource(id = R.drawable.ic_round_favorite_24),
                isSupportButton = true
            ) {}
            SettingsButton(
                text = "Resolution",
                rightDetail = "Large",
                iconPainter = painterResource(id = R.drawable.ic_round_photo_size_select_large_24)
            ) {}
            SettingsButton(
                text = "Automatic Lights",
                switchState = true,
                iconPainter = painterResource(id = R.drawable.ic_round_lightbulb_24)
            ) {}
            SettingsButton(
                text = "Automatic Lights long text test 124",
                rightDetail = "Some right detail",
                iconPainter = painterResource(id = R.drawable.ic_round_lightbulb_24)
            ) {}
            SettingsButton(
                text = "Automatic Lights",
                switchState = false,
                iconPainter = painterResource(id = R.drawable.ic_round_lightbulb_24),
                isLocked = true
            ) {}
        }
    }
}