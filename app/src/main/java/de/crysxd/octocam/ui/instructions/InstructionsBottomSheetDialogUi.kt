package de.crysxd.octocam.ui.instructions

import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.text.HtmlCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import de.crysxd.octocam.R
import de.crysxd.octocam.models.NetworkAddress
import de.crysxd.octocam.models.ServiceState
import de.crysxd.octocam.ui.base.Buttons
import de.crysxd.octocam.ui.base.Titles
import de.crysxd.octocam.ui.theme.*

class InstructionsBottomSheetDialogUi(
    val showHelp: () -> Unit
) {

    @Composable
    fun Layout(state: LiveData<ServiceState>) {
        Column(
            modifier = Modifier.padding(ScreenMargins),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            val s = state.observeAsState()

            Titles.SectionTitle(
                title = "OctoCam setup",
                modifier = Modifier.padding(bottom = Margin20)
            )

            Section("Open your OctoPrint webinterface, open the preferences and select <b>Webcam & Timelapse</b>.")
            ScreenShot(R.drawable.screenshot_1)
            Section("Enter one of the following into the <b>Stream URL</b> field:")
            ConnectOptions(state = s.value, path = "")
            Section("Click <b>Test</b> and make sure you see a image. If you don't see a image try another URL from the list above and make sure that this phone is on <b>the correct WiFi</b>!")
            ScreenShot(R.drawable.screenshot_2)
            Section("If you want to use OctoCam for timelapses, copy one of the following into the <b>Snapshot URL</b> field:")
            ConnectOptions(state = s.value, path = "?snapshot")
            Section(html = "Click <b>Test</b> and make sure you see a image. Now your are ready! Make sure to click <b>Save</b>, reload the webpage and you should see a camera image in the Control tab!")
            ScreenShot(R.drawable.screenshot_3)
            Buttons.Link(
                text = "Need help?",
                onClick = showHelp
            )
        }
    }

    @Composable
    fun Section(html: String, modifier: Modifier = Modifier) {
        val color = MaterialTheme.colors.normalText.toArgb()
        AndroidView(
            factory = { ctx ->
                TextView(ctx).also {
                    it.text = HtmlCompat.fromHtml(html, HtmlCompat.FROM_HTML_MODE_COMPACT)
                    it.setTextColor(color)
                }
            },
            modifier = modifier
                .padding(bottom = Margin10)
                .fillMaxWidth()
        )
    }

    @Composable
    fun ScreenShot(@DrawableRes screenshot: Int, modifier: Modifier = Modifier) {
        Image(
            painter = painterResource(screenshot),
            contentDescription = null,
            modifier = modifier
                .padding(bottom = Margin20)
                .fillMaxWidth()
        )
    }

    @Composable
    fun ConnectOptions(state: ServiceState?, path: String) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = Margin10)
        ) {
            state?.addresses?.sortedByDescending { it.quality }?.forEachIndexed { i, it ->
                Text(
                    style = Typography.caption,
                    text = "${state.protocol}://${it.address}:${state.port}/$path",
                    textAlign = TextAlign.Center,
                    color = MaterialTheme.colors.darkText,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = if (i == 0) 0.dp else Margin5)
                        .clip(MaterialTheme.shapes.medium)
                        .background(MaterialTheme.colors.cardBackground)
                        .padding(Margin5),
                )
            }
        }
    }
}

@Preview(showBackground = true, showSystemUi = true)
@Composable
fun SupportPreview() {
    OctoCamTheme {
        InstructionsBottomSheetDialogUi({}).Layout(
            MutableLiveData(
                ServiceState(
                    addresses = listOf(
                        NetworkAddress(10, "android.local"),
                        NetworkAddress(10, "android-2.local"),
                    ),
                    port = 5501,
                    protocol = "http",
                    connectionCount = 4,
                    lightsOn = true,
                )
            )
        )
    }
}