package de.crysxd.octocam.ui.instructions

import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.MediatorLiveData
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import de.crysxd.octocam.models.ServiceState
import de.crysxd.octocam.server.service.MainService
import de.crysxd.octocam.server.service.MainServiceBinder
import de.crysxd.octocam.ui.support.SupportBottomSheetDialog
import de.crysxd.octocam.ui.theme.OctoCamTheme
import timber.log.Timber

class InstructionsBottomSheetDialog : BottomSheetDialogFragment() {

    private var service: MainServiceBinder? = null
    private val state = MediatorLiveData<ServiceState>()
    private val ui = InstructionsBottomSheetDialogUi {
        SupportBottomSheetDialog().show(childFragmentManager, "support")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = NestedScrollView(requireContext()).also {
        val cv = ComposeView(requireContext())
        cv.setContent { OctoCamTheme { ui.Layout(state) } }
        it.addView(cv)
        it.isFillViewport = true
    }

    override fun onStart() {
        super.onStart()
        requireActivity().bindService(
            Intent(requireContext(), MainService::class.java),
            connection,
            0
        )
    }

    override fun onStop() {
        super.onStop()
        requireActivity().unbindService(connection)
    }

    /** Defines callbacks for service binding, passed to bindService()  */
    private val connection = object : ServiceConnection {

        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            Timber.i("Service connected")
            state.addSource((service as MainServiceBinder).getServiceState()) {
                state.postValue(it)
            }
        }

        override fun onServiceDisconnected(arg: ComponentName) {
            Timber.i("Service disconnected")
            service = null
        }
    }
}