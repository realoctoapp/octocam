package de.crysxd.octocam.ui.support

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import de.crysxd.octocam.ui.theme.OctoCamTheme

class SupportBottomSheetDialog : BottomSheetDialogFragment() {

    private val viewModel by lazy { ViewModelProvider(this)[SupportBottomSheetDialogViewModel::class.java] }
    private val ui = SupportBottomSheetDialogUi { message, includeLogs, includeInfo ->
        viewModel.sendMessage(requireActivity(), message, includeLogs, includeInfo)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = NestedScrollView(requireContext()).also {
        val cv = ComposeView(requireContext())
        cv.setContent { OctoCamTheme { ui.Layout() } }
        it.addView(cv)
        it.isFillViewport = true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.shouldDismiss.observe(viewLifecycleOwner) {
            if (it) {
                dismissAllowingStateLoss()
            }
        }
    }
}