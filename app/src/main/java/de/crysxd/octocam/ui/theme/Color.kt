package de.crysxd.octocam.ui.theme

import androidx.compose.material.Colors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

val White = Color(0xffffffff)
val Black = Color(0xFF000000)
val Red = Color(0xFFEE3F3F)
val RedTranslucent = Color(0x26EE3F3F)
val Blue = Color(0xFF389AE5)
val BlueTranslucent = Color(0x1A389AE5)
val DarkBlue = Color(0xFF104F80)
val DarkBlueTransparent = Color(0x1A104F80)
val Yellow = Color(0xFFFFC000)
val YellowTranslucent = Color(0x1FFFC000)
val Green = Color(0xFF27AE60)
val GreenTranslucent = Color(0x2627AE60)
val Orange = Color(0xFFFF9800)
val OrangeTranslucent = Color(0x26FF9800)
val Green2 = Color(0xFF13C100)
val Green3 = Color(0xFF249318)
val Violet = Color(0xFFBB6BD9)
val LightGrey = Color(0xFF999999)
val LightGreyTranslucent = Color(0x2D999999)
val VeryLightGrey = Color(0xFFeaeaea)
val DarkGrey = Color(0xFF1a1a1a)
val DarkGrey2 = Color(0xFF2B2A2A)
val LightBlue = Color(0xFFF0F9FF)
val BlackTranslucent = Color(0xB4000000)
val BlackTranslucent2 = Color(0x1A000000)
val BlackTranslucent3 = Color(0x0D000000)
val WhiteTranslucent = Color(0xB5FFFFFF)
val StatusBarScrim = Color(0x80FFFFFF)
val WhiteTranslucent2 = Color(0x2AFFFFFF)
val WhiteTranslucent3 = Color(0x12FFFFFF)


val Colors.lightText: Color
    @Composable get() = LightGrey

val Colors.darkText: Color
    @Composable get() = if (isLight) Black else White

val Colors.normalText: Color
    @Composable get() = if (isLight) BlackTranslucent else WhiteTranslucent

val Colors.inverseText: Color
    @Composable get() = White

val Colors.liveBadge: Color
    @Composable get() = Red

val Colors.settingsButtonForeground: Color
    @Composable get() = Yellow

val Colors.settingsButtonBackground: Color
    @Composable get() = YellowTranslucent

val Colors.supportButtonBackground: Color
    @Composable get() = RedTranslucent

val Colors.supportButtonForeground: Color
    @Composable get() = Red

val Colors.cardBackground: Color
    @Composable get() = LightBlue

val Colors.imageOverlay: Color
    @Composable get() = if (isLight) WhiteTranslucent else BlackTranslucent2

