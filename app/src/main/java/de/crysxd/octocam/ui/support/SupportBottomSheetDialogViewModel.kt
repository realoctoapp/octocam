package de.crysxd.octocam.ui.support

import android.app.Activity
import android.content.Intent
import android.content.res.Resources
import android.os.Build
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.core.os.ConfigurationCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.distinctUntilChanged
import androidx.lifecycle.viewModelScope
import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.remoteconfig.ktx.remoteConfig
import de.crysxd.octocam.R
import de.crysxd.octocam.logging.TimberCacheTree
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.json.JSONObject
import timber.log.Timber
import java.io.File
import java.util.*
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

class SupportBottomSheetDialogViewModel : ViewModel() {

    private val mutableShouldDismiss = MutableLiveData(false)
    val shouldDismiss = mutableShouldDismiss.distinctUntilChanged()

    fun sendMessage(
        activity: Activity,
        message: String,
        includeLogs: Boolean,
        includeInfo: Boolean,
    ) = viewModelScope.launch(Dispatchers.IO) {
        try {
            val (version, versionCode) = activity.packageManager.getPackageInfo(
                activity.packageName,
                0
            ).let {
                @Suppress("DEPRECATION")
                Pair(it.versionName, it.versionCode)
            }
            val subject = "Feedback OctoCam $version"
            val fcmToken = try {
                Tasks.await(FirebaseMessaging.getInstance().token)
            } catch (e: Exception) {
                Timber.w(e)
                "error"
            }
            val publicDir = File(
                activity.externalCacheDir,
                activity.getString(R.string.public_file_dir_name)
            )
            publicDir.mkdir()

            val zipFile = File(publicDir, "data.zip")
            val zipStream = ZipOutputStream(zipFile.outputStream())
            val zipFileUri = FileProvider.getUriForFile(
                activity,
                activity.applicationContext.packageName + ".provider",
                zipFile
            )
            var fileCount = 0

            if (includeLogs) {
                zipStream.putNextEntry(ZipEntry("logs.log"))
                zipStream.writer().apply {
                    write(TimberCacheTree.logs)
                    flush()
                }
                zipStream.closeEntry()
                fileCount++
            }

            if (includeInfo) {
                zipStream.putNextEntry(ZipEntry("phone_info.json"))
                val build = JSONObject()
                build.put("brand", Build.BRAND)
                build.put("manufacturer", Build.MANUFACTURER)
                build.put(
                    "languages",
                    ConfigurationCompat.getLocales(Resources.getSystem().configuration)
                        .toLanguageTags()
                )
                build.put("model", Build.MODEL)
                build.put("product", Build.PRODUCT)
                build.put("display", Build.DISPLAY)
                build.put("supported_abis", Build.SUPPORTED_ABIS.joinToString())
                build.put("supported_abis_32_bit", Build.SUPPORTED_32_BIT_ABIS.joinToString())
                build.put("supported_abis_64_bit", Build.SUPPORTED_64_BIT_ABIS.joinToString())
                val buildVersion = JSONObject()
                build.put("sdk", Build.VERSION.SDK_INT)
                build.put("release", Build.VERSION.RELEASE)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    build.put("preview_sdk", Build.VERSION.PREVIEW_SDK_INT)
                    build.put("security_patch", Build.VERSION.SECURITY_PATCH)
                }
                build.put("version", buildVersion)
                val appVersion = JSONObject()
                appVersion.put("version_name", version)
                appVersion.put("version_code", versionCode)
                appVersion.put("appid", Firebase.auth.currentUser?.uid ?: "")
                appVersion.put("fcmtoken", fcmToken)
                val locale = JSONObject()
                locale.put("language", Locale.getDefault().isO3Language)
                locale.put("country", Locale.getDefault().isO3Country)
                val phoneInfo = JSONObject()
                phoneInfo.put("phone", build)
                phoneInfo.put("app", appVersion)
                phoneInfo.put("locale", locale)

                zipStream.writer().apply {
                    write(phoneInfo.toString())
                    flush()
                }
                zipStream.closeEntry()
                fileCount++

                zipStream.close()

                val email = Firebase.remoteConfig.getString("contact_email")
                val intent = Intent(Intent.ACTION_SEND).also {
                    it.type = "message/rfc822"
                    it.putExtra(Intent.EXTRA_SUBJECT, subject)
                    it.putExtra(Intent.EXTRA_TEXT, message)
                    it.putExtra(Intent.EXTRA_EMAIL, arrayOf(email))

                    if (fileCount > 0) {
                        it.putExtra(Intent.EXTRA_STREAM, zipFileUri)
                    }
                }

                activity.startActivity(intent)
            }
        } catch (e: Exception) {
            Timber.e(e)
            Toast.makeText(activity, "Something went wrong", Toast.LENGTH_SHORT).show()
        } finally {
            mutableShouldDismiss.postValue(true)
        }
    }
}