package de.crysxd.octocam

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import de.crysxd.octocam.repository.OctoCamSettingsRepository
import de.crysxd.octocam.server.service.MainService
import timber.log.Timber

class BootCompletedReceiver : BroadcastReceiver() {


    override fun onReceive(context: Context, intent: Intent) {
        val isBoot = intent.action == Intent.ACTION_BOOT_COMPLETED
        val shouldStart = OctoCamSettingsRepository.value.startAfterBoot
        if (isBoot && shouldStart) {
            try {
                Timber.i("Boot completed, starting MainService")
                MainService.start(context)
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }
}