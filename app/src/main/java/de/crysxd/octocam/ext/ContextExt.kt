package de.crysxd.octocam.ext

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat

val Context.requiredPermissions get() = arrayOf(Manifest.permission.CAMERA)

fun Context.allPermissionsGranted() = requiredPermissions.all {
    ContextCompat.checkSelfPermission(this, it) == PackageManager.PERMISSION_GRANTED
}