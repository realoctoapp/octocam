package de.crysxd.octocam.ext

import androidx.lifecycle.MutableLiveData

fun <X> MutableLiveData<X>.postStateUpdate(update: X.() -> X) {
    value = value?.let(update)
}