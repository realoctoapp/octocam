package de.crysxd.octocam.repository

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.distinctUntilChanged
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import de.crysxd.octocam.models.OctoCamSettings
import timber.log.Timber


object OctoCamSettingsRepository {

    private val moshi = Moshi.Builder().addLast(KotlinJsonAdapterFactory()).build()
    private var jsonAdapter = moshi.adapter(OctoCamSettings::class.java)

    private const val KEY_PREFS = "octocam_settings"
    private val mutableLiveData = MutableLiveData<OctoCamSettings>()
    private lateinit var sharedPrefs: SharedPreferences
    private val defaults = OctoCamSettings()
    val liveValue = mutableLiveData.distinctUntilChanged()
    var value: OctoCamSettings
        get() = sharedPrefs.getString(KEY_PREFS, null)?.let {
            val settings = jsonAdapter.fromJson(it)
            mutableLiveData.postValue(settings ?: defaults)
            settings
        } ?: defaults
        set(new) {
            if (new == value) return
            Timber.i("New settings: $new")
            sharedPrefs.edit { putString(KEY_PREFS, jsonAdapter.toJson(new)) }
            mutableLiveData.postValue(new)
        }


    fun init(context: Context) {
        sharedPrefs = context.getSharedPreferences("octocam_settings", Context.MODE_PRIVATE)
    }

    fun update(update: OctoCamSettings.() -> OctoCamSettings) {
        value = value.update()
    }
}